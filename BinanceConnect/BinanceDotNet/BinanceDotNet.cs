﻿using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using BinanceModel;
using BinanceRequest;
using BinanceRequest.ApiRequests;

namespace BinanceDotNetApi
{
    public class BinanceDotNet
    {
        private readonly IBinanceRequest _request;
        private readonly IMapper _mapper;
        public BinanceDotNet(IBinanceRequest request, IMapper mapper)
        {
            _request = request;
            _mapper = mapper;
        }

        public async Task<ExchangeInfo> ExchangeInfo()
        {
            var response = await _request.SendRequest(new ExchangeInfoRequest());

            var document = JsonDocument.Parse(response);
            var data = _mapper.Map<ExchangeInfo>(document);
            
            return data;
        }
    }
}
