﻿using AutoMapper;
using BinanceMapping;
using BinanceRequest;
using Microsoft.Extensions.DependencyInjection;

namespace BinanceDotNetApi
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddBinanceDotNet(this IServiceCollection services)
        {
            return services
                .AddHttpClient()
                .AddSingleton(typeof(BinanceDotNet))
                .AddSingleton<IBinanceRequest, BinanceRequest.BinanceRequest>()
                .AddSingleton(typeof(BinanceKeys))
                .AddAutoMapper(typeof(BinanceProfile));
        }
    }
}
