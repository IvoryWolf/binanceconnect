﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using AutoMapper;
using BinanceMapping.Converters;
using BinanceModel;
using static BinanceMapping.Converters.BinanceEnumConverter;

namespace BinanceMapping
{
    //AutoMapper profile where conversions from Binance API calls are mapped to BinanceDotNet classes.
    public class BinanceProfile : Profile
    {
        public BinanceProfile()
        {
            AddKlinesMappings();

            AddPricesMappings();

            AddAggregateTradeMappings();

            AddTimeMappings();

            AddAveragePriceMappings();

            AddBookTickerMappings();

            AddChanges24HrMappings();

            AddHistoricalTradeMapping();

            AddOrderBookMapping();

            AddRecentTradeMappings();

            AddRateLimitMappings();

            AddFilterMappings();

            AddSymbolMappings();

            AddExchangeInfoMappings();
        }

        private void AddKlinesMappings()
        {
            //Klines 
            CreateMap<JsonDocument, IEnumerable<Kline>>().ConvertUsing<BinanceKlinesToListOfKlineConverter>();
            CreateMap<JsonElement.ArrayEnumerator, Kline>()
                .ForCtorParam("openTime", y => y.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.ElementAt((int) KlineFields.OpenTime).GetInt64()).UtcDateTime))
                .ForCtorParam("open", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.Open).GetString())))
                .ForCtorParam("high", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.High).GetString())))
                .ForCtorParam("low", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.Low).GetString())))
                .ForCtorParam("close", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.Close).GetString())))
                .ForCtorParam("volume", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.Volume).GetString())))
                .ForCtorParam("closeTime", y => y.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.ElementAt((int) KlineFields.CloseTime).GetInt64()).UtcDateTime))
                .ForCtorParam("quoteAssetVolume", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.QuoteAssetVolume).GetString())))
                .ForCtorParam("numberOfTrades", y => y.MapFrom(j => j.ElementAt((int) KlineFields.NumberOfTrades).GetInt64()))
                .ForCtorParam("takerBuyBaseAssetVolume", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.TakerBuyBaseAssetVolume).GetString())))
                .ForCtorParam("takerBuyQuoteAssetVolume", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.TakerBuyQuoteAssetVolume).GetString())))
                .ForCtorParam("ignore", y => y.MapFrom(j => decimal.Parse(j.ElementAt((int) KlineFields.Ignore).GetString())));
        }

        private void AddPricesMappings()
        {
            //Prices 
            CreateMap<JsonDocument, IEnumerable<Price>>().ConvertUsing<BinanceItemsToItemListConverter<JsonDocument, Price>>();
            CreateMap<JsonElement, Price>().ForCtorParam("symbol", src => src.MapFrom(j => j.GetProperty("symbol")))
                .ForCtorParam("value", src => src.MapFrom(j => decimal.Parse(j.GetProperty("price").ToString())));
        }

        private void AddAggregateTradeMappings()
        {
            //AggregateTrades 
            CreateMap<JsonDocument, IEnumerable<AggregateTrade>>()
                .ConvertUsing<BinanceItemsToItemListConverter<JsonDocument, AggregateTrade>>();
            CreateMap<JsonElement, AggregateTrade>()
                .ForCtorParam("tradeId", src => src.MapFrom(j => j.GetProperty("a").GetInt64()))
                .ForCtorParam("price", src => src.MapFrom(j => decimal.Parse(j.GetProperty("p").ToString())))
                .ForCtorParam("quantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("q").ToString())))
                .ForCtorParam("firstTradeId", src => src.MapFrom(j => j.GetProperty("f").GetInt64()))
                .ForCtorParam("lastTradeId", src => src.MapFrom(j => j.GetProperty("l").GetInt64()))
                .ForCtorParam("timeStamp", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("T").GetInt64()).UtcDateTime))
                .ForCtorParam("buyerWasTheMaker", src => src.MapFrom(j => j.GetProperty("m").GetBoolean()))
                .ForCtorParam("tradeWasBestPriceMatch", src => src.MapFrom(j => j.GetProperty("M").GetBoolean()));
        }

        private void AddTimeMappings()
        {
            //Time 
            CreateMap<JsonDocument, Time>().ForCtorParam("serverTime", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.RootElement.GetProperty("serverTime").GetInt64()).UtcDateTime));
        }

        private void AddAveragePriceMappings()
        {
            //Average Price 
            CreateMap<JsonDocument, AveragePrice>()
                .ForCtorParam("minutes", src => src.MapFrom(j => j.RootElement.GetProperty("mins").GetInt32()))
                .ForCtorParam("price", src => src.MapFrom(j => decimal.Parse(j.RootElement.GetProperty("price").GetString())));
        }

        private void AddBookTickerMappings()
        {
            //BookTicker mappings
            CreateMap<JsonDocument, IEnumerable<BookTicker>>().ConvertUsing<BinanceBookTickersToListOfBookTickerConverter>();
            CreateMap<JsonElement, BookTicker>()
                .ForCtorParam("symbol", src => src.MapFrom(j => j.GetProperty("symbol").GetString()))
                .ForCtorParam("bidPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("bidPrice").GetString())))
                .ForCtorParam("bidQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("bidQty").GetString())))
                .ForCtorParam("askPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("askPrice").GetString())))
                .ForCtorParam("askQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("askQty").GetString())));
        }

        private void AddChanges24HrMappings()
        {
            //Changes24Hr 
            CreateMap<JsonDocument, Changes24Hr>().ConvertUsing<Binance24HrToChange24HrConverter>();
            CreateMap<JsonElement, Changes24Hr>()
                .ForCtorParam("symbol", src => src.MapFrom(j => j.GetProperty("symbol").GetString()))
                .ForCtorParam("priceChange", src => src.MapFrom(j => decimal.Parse(j.GetProperty("priceChange").ToString())))
                .ForCtorParam("priceChangePercent", src => src.MapFrom(j => decimal.Parse(j.GetProperty("priceChangePercent").GetString())))
                .ForCtorParam("weightedAveragePrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("weightedAvgPrice").GetString())))
                .ForCtorParam("previousClosePrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("prevClosePrice").GetString())))
                .ForCtorParam("lastPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("lastPrice").GetString())))
                .ForCtorParam("lastQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("lastQty").GetString())))
                .ForCtorParam("bidPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("bidPrice").GetString())))
                .ForCtorParam("bidQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("bidQty").GetString())))
                .ForCtorParam("askPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("askPrice").GetString())))
                .ForCtorParam("askQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("askQty").GetString())))
                .ForCtorParam("openPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("openPrice").GetString())))
                .ForCtorParam("highPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("highPrice").GetString())))
                .ForCtorParam("lowPrice", src => src.MapFrom(j => decimal.Parse(j.GetProperty("lowPrice").GetString())))
                .ForCtorParam("volume", src => src.MapFrom(j => decimal.Parse(j.GetProperty("volume").GetString())))
                .ForCtorParam("quoteVolume", src => src.MapFrom(j => decimal.Parse(j.GetProperty("quoteVolume").GetString())))
                .ForCtorParam("openTime", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("openTime").GetInt64()).UtcDateTime))
                .ForCtorParam("closeTime", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("closeTime").GetInt64()).UtcDateTime))
                .ForCtorParam("firstTradeId", src => src.MapFrom(j => j.GetProperty("firstId").GetInt64()))
                .ForCtorParam("lastTradeId", src => src.MapFrom(j => j.GetProperty("lastId").GetInt64()))
                .ForCtorParam("count", src => src.MapFrom(j => j.GetProperty("count").GetInt64()));
        }

        private void AddHistoricalTradeMapping()
        {
            // HistoricalTrade
            CreateMap<JsonDocument, IEnumerable<HistoricalTrade>>()
                .ConvertUsing<BinanceItemsToItemListConverter<JsonDocument, HistoricalTrade>>();
            CreateMap<JsonElement, HistoricalTrade>()
                .ForCtorParam("tradeId", src => src.MapFrom(j => j.GetProperty("id").GetInt64()))
                .ForCtorParam("price", src => src.MapFrom(j => decimal.Parse(j.GetProperty("price").ToString())))
                .ForCtorParam("quantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("qty").ToString())))
                .ForCtorParam("quoteQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("quoteQty").ToString())))
                .ForCtorParam("time", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("time").GetInt64()).UtcDateTime))
                .ForCtorParam("isBuyerMaker", src => src.MapFrom(j => j.GetProperty("isBuyerMaker").GetBoolean()))
                .ForCtorParam("isBestMatch", src => src.MapFrom(j => j.GetProperty("isBestMatch").GetBoolean()));
        }

        private void AddOrderBookMapping()
        {
            // OrderBook
            CreateMap<JsonDocument, OrderBook>().ConvertUsing<BinanceDepthToOrderBookConverter>();
            CreateMap<JsonElement, IEnumerable<Order>>().ConvertUsing<BinanceOrdersToListOfOrderConverter>();
            CreateMap<JsonElement.ArrayEnumerator, Order>()
                .ForCtorParam("price", src => src.MapFrom(j => decimal.Parse(j.ElementAt(0).GetString())))
                .ForCtorParam("quantity", src => src.MapFrom(j => decimal.Parse(j.ElementAt(1).GetString())));
        }

        private void AddRecentTradeMappings()
        {
            // RecentTrades
            CreateMap<JsonDocument, IEnumerable<RecentTrade>>()
                .ConvertUsing<BinanceItemsToItemListConverter<JsonDocument, RecentTrade>>();
            CreateMap<JsonElement, RecentTrade>()
                .ForCtorParam("tradeId", src => src.MapFrom(j => j.GetProperty("id").GetInt64()))
                .ForCtorParam("price", src => src.MapFrom(j => decimal.Parse(j.GetProperty("price").GetString())))
                .ForCtorParam("quantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("qty").GetString())))
                .ForCtorParam("quoteQuantity", src => src.MapFrom(j => decimal.Parse(j.GetProperty("quoteQty").GetString())))
                .ForCtorParam("time", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("time").GetInt64()).UtcDateTime))
                .ForCtorParam("isBuyerMaker", src => src.MapFrom(j => j.GetProperty("isBuyerMaker").GetBoolean()))
                .ForCtorParam("isBestMatch", src => src.MapFrom(j => j.GetProperty("isBestMatch").GetBoolean()));
        }

        private void AddRateLimitMappings()
        {
            // RateLimits
            CreateMap<JsonElement, IEnumerable<RateLimit>>()
                .ConvertUsing<BinanceItemsToItemListConverter<JsonElement, RateLimit>>();
            CreateMap<JsonElement, RateLimit>()
                .ForCtorParam("type", src => src.MapFrom(j => RateLimitTypes[j.GetProperty("rateLimitType").GetString()]))
                .ForCtorParam("interval", src => src.MapFrom(j => RateLimitIntervals[j.GetProperty("interval").GetString()]))
                .ForCtorParam("intervalValue", src => src.MapFrom(j => j.GetProperty("intervalNum").GetInt32()))
                .ForCtorParam("limit", src => src.MapFrom(j => j.GetProperty("limit").GetInt32()));
        }

        private void AddFilterMappings()
        {
            // Filters (Exchange and Symbol)
            CreateMap<JsonElement, IEnumerable<Filter>>().ConvertUsing<BinanceItemsToItemListConverter<JsonElement, Filter>>();
            CreateMap<JsonElement, Filter>().ConvertUsing<BinanceFilterToFilterConverter>();
        }

        private void AddSymbolMappings()
        {
            // Symbol 
            CreateMap<JsonElement, IEnumerable<Symbol>>().ConvertUsing<BinanceItemsToItemListConverter<JsonElement, Symbol>>();
            CreateMap<JsonElement, Symbol>()
                .ForCtorParam("ticker", src => src.MapFrom(j => j.GetProperty("symbol").GetString()))
                .ForCtorParam("status", src => src.MapFrom(j => SymbolStatuses[j.GetProperty("status").GetString()]))
                .ForCtorParam("baseAsset", src => src.MapFrom(j => j.GetProperty("baseAsset").GetString()))
                .ForCtorParam("baseAssetPrecision", src => src.MapFrom(j => j.GetProperty("baseAssetPrecision").GetInt32()))
                .ForCtorParam("quoteAsset", src => src.MapFrom(j => j.GetProperty("quoteAsset").GetString()))
                .ForCtorParam("quoteAssetPrecision", src => src.MapFrom(j => j.GetProperty("quoteAssetPrecision").GetInt32()))
                .ForCtorParam("quotePrecision", src => src.MapFrom(j => j.GetProperty("quotePrecision").GetInt32()))
                .ForCtorParam("baseCommissionPrecision", src => src.MapFrom(j => j.GetProperty("baseCommissionPrecision").GetInt32()))
                .ForCtorParam("quoteCommissionPrecision", src => src.MapFrom(j => j.GetProperty("quoteCommissionPrecision").GetInt32()))
                .ForCtorParam("icebergAllowed", src => src.MapFrom(j => j.GetProperty("icebergAllowed").GetBoolean()))
                .ForCtorParam("ocoAllowed", src => src.MapFrom(j => j.GetProperty("ocoAllowed").GetBoolean()))
                .ForCtorParam("quoteOrderQuantityMarketAllowed", src => src.MapFrom(j => j.GetProperty("quoteOrderQtyMarketAllowed").GetBoolean()))
                .ForCtorParam("isSpotTradingAllowed", src => src.MapFrom(j => j.GetProperty("isSpotTradingAllowed").GetBoolean()))
                .ForCtorParam("isMarginTradingAllowed", src => src.MapFrom(j => j.GetProperty("isMarginTradingAllowed").GetBoolean()))
                .ForCtorParam("filters", src => src.MapFrom(j => j.GetProperty("filters")))
                .ForCtorParam("permissions", src => src.MapFrom(j => j.GetProperty("permissions").EnumerateArray().ToList().Select(p => Permissions[p.GetString()])))
                .ForCtorParam("orderTypes", src => src.MapFrom(j => j.GetProperty("orderTypes").EnumerateArray().ToList().Select(o => OrderTypes[o.GetString()])));
        }

        private void AddExchangeInfoMappings()
        {
            // Exchange Info
            CreateMap<JsonDocument, ExchangeInfo>().ConvertUsing<BinanceExchangeInfoToExchangeInfo>();
            CreateMap<JsonElement, ExchangeInfo>()
                .ForCtorParam("timezone", src => src.MapFrom(j => j.GetProperty("timezone").GetString()))
                .ForCtorParam("serverTime", src => src.MapFrom(j => DateTimeOffset.FromUnixTimeMilliseconds(j.GetProperty("serverTime").GetInt64()).UtcDateTime))
                .ForCtorParam("rateLimits", src => src.MapFrom(j => j.GetProperty("rateLimits")))
                .ForCtorParam("exchangeFilters", src => src.MapFrom(j => j.GetProperty("exchangeFilters")))
                .ForCtorParam("symbols", src => src.MapFrom(j => j.GetProperty("symbols")));
        }
        
    }
}
