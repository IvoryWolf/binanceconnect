﻿using System.Text.Json;
using AutoMapper;
using BinanceModel;

namespace BinanceMapping.Converters
{
    public class Binance24HrToChange24HrConverter : ITypeConverter<JsonDocument, Changes24Hr>
    {
        public Changes24Hr Convert(JsonDocument source, Changes24Hr destination, ResolutionContext context)
        {
            return context.Mapper.Map<JsonElement, Changes24Hr>(source.RootElement);
        }
    }
}
