﻿using System.Collections.Generic;
using System.Text.Json;
using AutoMapper;
using BinanceModel;

namespace BinanceMapping.Converters
{
    public class BinanceBookTickersToListOfBookTickerConverter : ITypeConverter<JsonDocument, IEnumerable<BookTicker>>
    {
        public IEnumerable<BookTicker> Convert(JsonDocument source, IEnumerable<BookTicker> destination, ResolutionContext context)
        {
            var items = new List<BookTicker>();
            if (source.RootElement.ValueKind == JsonValueKind.Array)
            {
                foreach (var element in source.RootElement.EnumerateArray())
                {
                    var item = context.Mapper.Map<JsonElement, BookTicker>(element);
                    items.Add(item);
                }
                return items;
            }

            var singleItem = context.Mapper.Map<JsonElement, BookTicker>(source.RootElement);
            items.Add(singleItem);
            return items;

        }
    }
}
