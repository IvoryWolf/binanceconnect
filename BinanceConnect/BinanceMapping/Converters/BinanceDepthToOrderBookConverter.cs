﻿using System.Collections.Generic;
using System.Text.Json;
using AutoMapper;
using BinanceModel;

namespace BinanceMapping.Converters
{
    class BinanceDepthToOrderBookConverter : ITypeConverter<JsonDocument, OrderBook>
    {
        public OrderBook Convert(JsonDocument source, OrderBook destination, ResolutionContext context)
        {
            var bidsElement = source.RootElement.GetProperty("bids");
            var askElement = source.RootElement.GetProperty("asks");

            var bids = context.Mapper.Map<JsonElement, List<Order>>(bidsElement);
            var asks = context.Mapper.Map<JsonElement, List<Order>>(askElement);

            return new OrderBook(source.RootElement.GetProperty("lastUpdateId").GetInt64(), bids, asks);
        }
    }

    public class BinanceOrdersToListOfOrderConverter : ITypeConverter<JsonElement, IEnumerable<Order>>
    {
        public IEnumerable<Order> Convert(JsonElement source, IEnumerable<Order> destination, ResolutionContext context)
        {
            var items = new List<Order>();

            foreach (var orders in source.EnumerateArray())
            {
                var item = context.Mapper.Map<JsonElement.ArrayEnumerator, Order>(orders.EnumerateArray());
                items.Add(item);
            }

            return items;
        }
    }
}
