﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using BinanceModel;

namespace BinanceMapping.Converters
{
    public static class BinanceEnumConverter
    {
        public static readonly Dictionary<string, RateLimitType> RateLimitTypes = new Dictionary<string, RateLimitType>()
        {
            {"REQUEST_WEIGHT", RateLimitType.RequestWeight},
            {"ORDERS", RateLimitType.Orders},
            {"RAW_REQUESTS", RateLimitType.RawRequests}
        };

        public static readonly Dictionary<string, RateLimitInterval> RateLimitIntervals = new Dictionary<string, RateLimitInterval>()
        {
            {"SECOND", RateLimitInterval.Second},
            {"MINUTE", RateLimitInterval.Minute},
            {"DAY", RateLimitInterval.Day}
        };

        public static readonly Dictionary<string, FilterType> FilterTypes = new Dictionary<string, FilterType>()
        {
           {"EXCHANGE_MAX_NUM_ORDERS", FilterType.ExchangeMaxNumOrders},
           {"EXCHANGE_MAX_ALGO_ORDERS", FilterType.ExchangeMaxAlgoOrders},
           {"PRICE_FILTER", FilterType.PriceFilter},
           {"PERCENT_PRICE", FilterType.PercentPrice},
           {"LOT_SIZE", FilterType.LotSize},
           {"MIN_NOTIONAL", FilterType.MinNotional},
           {"ICEBERG_PARTS", FilterType.IcebergParts},
           {"MARKET_LOT_SIZE", FilterType.MarketLotSize},
           {"MAX_NUM_ORDERS", FilterType.MaxNumOrders},
           {"MAX_NUM_ALGO_ORDERS", FilterType.MaxNumAlgoOrders},
           {"MAX_NUM_ICEBERG_ORDERS", FilterType.MaxNumIcebergOrders},
           {"MAX_POSITION", FilterType.MaxPosition}
        };

        public static readonly Dictionary<string, SymbolStatus> SymbolStatuses = new Dictionary<string, SymbolStatus>()
        {
            {"PRE_TRADING", SymbolStatus.PreTrading},
            {"TRADING", SymbolStatus.Trading},
            {"POST_TRADING", SymbolStatus.PostTrading},
            {"END_OF_DAY", SymbolStatus.EndOfDay},
            {"HALT", SymbolStatus.Halt},
            {"AUCTION_MATCH", SymbolStatus.AuctionMatch},
            {"BREAK", SymbolStatus.Break}
        };

        public static readonly Dictionary<string, Permission> Permissions = new Dictionary<string, Permission>()
        {
            {"MARGIN", Permission.Margin},
            {"SPOT", Permission.Spot},
            {"LEVERAGED", Permission.Leveraged}
        };

        public static readonly Dictionary<string, OrderType> OrderTypes = new Dictionary<string, OrderType>()
        {
            {"LIMIT", OrderType.Limit},
            {"LIMIT_MAKER", OrderType.LimitMaker},
            {"MARKET", OrderType.Market},
            {"STOP_LOSS", OrderType.StopLoss},
            {"STOP_LOSS_LIMIT", OrderType.StopLossLimit},
            {"TAKE_PROFIT", OrderType.TakeProfit},
            {"TAKE_PROFIT_LIMIT", OrderType.TakeProfitLimit},
        };
    }
}
