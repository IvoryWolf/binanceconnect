﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using AutoMapper;
using BinanceModel;

namespace BinanceMapping.Converters
{
    public class BinanceExchangeInfoToExchangeInfo : ITypeConverter<JsonDocument, ExchangeInfo>
    {
        public ExchangeInfo Convert(JsonDocument source, ExchangeInfo destination, ResolutionContext context)
        {
            var rootElement = source.RootElement;
            var symbolArray = source.RootElement.GetProperty("symbols").EnumerateArray();

            return context.Mapper.Map<JsonElement, ExchangeInfo>(rootElement);
        }
    }
}
