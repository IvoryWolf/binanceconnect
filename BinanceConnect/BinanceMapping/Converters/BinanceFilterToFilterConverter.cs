﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using AutoMapper;
using BinanceModel;
using static BinanceMapping.Converters.BinanceEnumConverter;

namespace BinanceMapping.Converters
{
    public class BinanceFilterToFilterConverter : ITypeConverter<JsonElement, Filter>
    {
        public Filter Convert(JsonElement source, Filter destination, ResolutionContext context)
        {
            var enumeratedObject = source.EnumerateObject();

            enumeratedObject.MoveNext();
            var filterType = FilterTypes[enumeratedObject.Current.Value.GetString()];
            var fields = new Dictionary<string, object>();
            foreach(var item in enumeratedObject)
            {
                // For some reason binance represents decimals as strings in the json
                // so we have to check the kind and see if it can be parsed to a decimal.
                if (item.Value.ValueKind == JsonValueKind.String)
                {
                    if (decimal.TryParse(item.Value.GetString(), out var decimalValue))
                    {
                        fields.Add(item.Name, decimalValue);
                        continue;
                    }

                    fields.Add(item.Name, item.Value.GetString());
                }
                else
                {
                    if (item.Value.ValueKind == JsonValueKind.Number)
                    {
                        fields.Add(item.Name, item.Value.GetInt64());
                    }
                    else if (item.Value.ValueKind == JsonValueKind.True || item.Value.ValueKind == JsonValueKind.False)
                    {
                        fields.Add(item.Name, item.Value.GetBoolean());
                    }
                    else
                    {
                        fields.Add(item.Name, null);
                    }
                }
                
            }

            return new Filter(filterType, fields);
        }
    }
}
