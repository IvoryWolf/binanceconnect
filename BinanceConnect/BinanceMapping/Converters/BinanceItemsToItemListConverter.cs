﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using System.Text.Json;
using AutoMapper;

namespace BinanceMapping.Converters
{
    // Wanted to add generic parameter constraints but these classes are sealed and don't implement a suitable interface
    public class BinanceItemsToItemListConverter<TSource, TDest> : ITypeConverter<TSource, IEnumerable<TDest>> 
    {
        public IEnumerable<TDest> Convert(TSource source, IEnumerable<TDest> destination, ResolutionContext context)
        {
            return source switch
            {
                JsonDocument document => ExtractListFromArray(document.RootElement.EnumerateArray(), context),
                JsonElement element => ExtractListFromArray(element.EnumerateArray(), context),
                JsonElement.ArrayEnumerator array => ExtractListFromArray(array, context),
                _ => null
            };
        }

        private IEnumerable<TDest> ExtractListFromArray(JsonElement.ArrayEnumerator source, ResolutionContext context)
        {
            var items = new List<TDest>();
            foreach (var element in source)
            {
                var item = context.Mapper.Map<JsonElement, TDest>(element);
                items.Add(item);
            }

            return items;
        }

    }
}
