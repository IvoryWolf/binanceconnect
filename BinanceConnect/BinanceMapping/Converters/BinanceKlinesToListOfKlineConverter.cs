﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using AutoMapper;
using BinanceModel;

namespace BinanceMapping.Converters
{
    public class BinanceKlinesToListOfKlineConverter : ITypeConverter<JsonDocument, IEnumerable<Kline>>
    {
        public IEnumerable<Kline> Convert(JsonDocument source, IEnumerable<Kline> destination, ResolutionContext context)
        {
            var klines = new List<Kline>();
            foreach (var element in source.RootElement.EnumerateArray())
            {
                var price = context.Mapper.Map<JsonElement.ArrayEnumerator, Kline>(element.EnumerateArray());
                klines.Add(price);
            }

            return klines;
        }
    }
}
