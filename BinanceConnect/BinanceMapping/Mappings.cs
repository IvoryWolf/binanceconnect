﻿using System;

namespace BinanceMapping
{
    public class Mappings
    {

    }

    public enum KlineFields
    {
        OpenTime,
        Open,
        High,
        Low,
        Close,
        Volume,
        CloseTime,
        QuoteAssetVolume,
        NumberOfTrades,
        TakerBuyBaseAssetVolume,
        TakerBuyQuoteAssetVolume,
        Ignore
    }
}
