﻿using System;

namespace BinanceModel
{
    /*
      {
        "a": 26129,         // Aggregate tradeId
        "p": "0.01633102",  // Price
        "q": "4.70443515",  // Quantity
        "f": 27781,         // First tradeId
        "l": 27781,         // Last tradeId
        "T": 1498793709153, // Timestamp
        "m": true,          // Was the buyer the maker?
        "M": true           // Was the trade the best price match?
      }
    */

    public class AggregateTrade
    {
        public AggregateTrade(long tradeId, decimal price, decimal quantity, long firstTradeId, long lastTradeId, DateTime timeStamp, bool buyerWasTheMaker, bool tradeWasBestPriceMatch)
        {
            TradeId = tradeId;
            Price = price;
            Quantity = quantity;
            FirstTradeId = firstTradeId;
            LastTradeId = lastTradeId;
            TimeStamp = timeStamp;
            BuyerWasTheMaker = buyerWasTheMaker;
            TradeWasBestPriceMatch = tradeWasBestPriceMatch;
        }

        public long TradeId { get; }
        public decimal Price { get; }
        public decimal Quantity { get; }
        public long FirstTradeId { get; }
        public long LastTradeId { get; }
        public DateTime TimeStamp { get; }
        public bool BuyerWasTheMaker { get; }
        public bool TradeWasBestPriceMatch { get; }
    }
}
