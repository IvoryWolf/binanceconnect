﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinanceModel
{
    public class AveragePrice
    {
        public AveragePrice(int minutes, decimal price)
        {
            Minutes = minutes;
            Price = price;
        }

        public int Minutes { get; }
        public decimal Price { get; }
    }
}
