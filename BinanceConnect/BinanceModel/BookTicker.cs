﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinanceModel
{
    public class BookTicker
    {
        public BookTicker(string symbol, decimal bidPrice, decimal bidQuantity, decimal askPrice, decimal askQuantity)
        {
            Symbol = symbol;
            BidPrice = bidPrice;
            BidQuantity = bidQuantity;
            AskPrice = askPrice;
            AskQuantity = askQuantity;
        }

        public string Symbol { get; }
        public decimal BidPrice { get; }
        public decimal BidQuantity { get; }
        public decimal AskPrice { get; }
        public  decimal AskQuantity { get; }
    }

}
