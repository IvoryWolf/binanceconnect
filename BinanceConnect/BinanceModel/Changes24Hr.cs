﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace BinanceModel
{
    public class Changes24Hr
    {
        public Changes24Hr(string symbol, decimal priceChange, decimal priceChangePercent, decimal weightedAveragePrice, decimal previousClosePrice, decimal lastPrice, decimal lastQuantity, decimal bidPrice, decimal bidQuantity, decimal askPrice, decimal askQuantity, decimal openPrice, decimal highPrice, decimal lowPrice, decimal volume, decimal quoteVolume, DateTime openTime, DateTime closeTime, long firstTradeId, long lastTradeId, long count)
        {
            Symbol = symbol;
            PriceChange = priceChange;
            PriceChangePercent = priceChangePercent;
            WeightedAveragePrice = weightedAveragePrice;
            PreviousClosePrice = previousClosePrice;
            LastPrice = lastPrice;
            LastQuantity = lastQuantity;
            BidPrice = bidPrice;
            BidQuantity = bidQuantity;
            AskPrice = askPrice;
            AskQuantity = askQuantity;
            OpenPrice = openPrice;
            HighPrice = highPrice;
            LowPrice = lowPrice;
            Volume = volume;
            QuoteVolume = quoteVolume;
            OpenTime = openTime;
            CloseTime = closeTime;
            FirstTradeId = firstTradeId;
            LastTradeId = lastTradeId;
            Count = count;
            
        }

        public string Symbol { get; }
        public decimal PriceChange { get; }
        public decimal PriceChangePercent { get; }
        public decimal WeightedAveragePrice { get; private set; }
        public decimal PreviousClosePrice { get; }
        public decimal LastPrice { get; }
        public decimal LastQuantity { get; }
        public decimal BidPrice { get; }
        public decimal BidQuantity { get; }
        public decimal AskPrice { get; }
        public decimal AskQuantity { get; }
        public decimal OpenPrice { get; }
        public decimal HighPrice { get; }
        public decimal LowPrice { get; }
        public decimal Volume { get; }
        public decimal QuoteVolume { get; }
        public DateTime OpenTime { get; }
        public DateTime CloseTime { get; }
        public long FirstTradeId { get; }
        public long LastTradeId { get; }
        public long Count { get; }
    }
}
