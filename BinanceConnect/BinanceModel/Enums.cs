﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinanceModel
{
    public enum SymbolStatus
    {
        PreTrading,
        Trading,
        PostTrading,
        EndOfDay,
        Halt,
        AuctionMatch,
        Break,
    }

    public enum RateLimitType
    {

        RequestWeight,
        Orders,
        RawRequests
    }

    public enum RateLimitInterval
    {
        Second,
        Minute,
        Day
    }

    public enum Permission
    {
        Spot,
        Margin,
        Leveraged
    }

    public enum OrderType
    {
        Limit,
        Market,
        StopLoss,
        StopLossLimit,
        TakeProfit,
        TakeProfitLimit,
        LimitMaker
    }
}
