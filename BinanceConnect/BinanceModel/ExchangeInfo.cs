﻿using System;
using System.Collections.Generic;

namespace BinanceModel
{
    public class ExchangeInfo
    {
        public ExchangeInfo(string timezone, DateTime serverTime, IEnumerable<RateLimit> rateLimits, IEnumerable<Filter> exchangeFilters, IEnumerable<Symbol> symbols)
        {
            Timezone = timezone;
            ServerTime = serverTime;
            RateLimits = rateLimits;
            ExchangeFilters = exchangeFilters;
            Symbols = symbols;
        }

        public string Timezone { get; }
        public DateTime ServerTime { get; }
        public IEnumerable<RateLimit> RateLimits { get; }
        public IEnumerable<Filter> ExchangeFilters { get; }
        public IEnumerable<Symbol> Symbols { get; }
    }
}
