﻿using System.Collections.Generic;

namespace BinanceModel
{
    public class Filter
    {
        
        public Filter(FilterType filterType, Dictionary<string, object> fields)
        {
            FilterType = filterType;
            Fields = fields;
        }

        public FilterType FilterType { get; }

        // This is essentially a name value pairing.
        public Dictionary<string, object> Fields { get; }

    };

    public enum FilterType
    {
        ExchangeMaxNumOrders,
        ExchangeMaxAlgoOrders,
        PriceFilter,
        PercentPrice,
        LotSize,
        MinNotional,
        IcebergParts,
        MarketLotSize,
        MaxNumOrders,
        MaxNumAlgoOrders,
        MaxNumIcebergOrders,
        MaxPosition
    }

}



