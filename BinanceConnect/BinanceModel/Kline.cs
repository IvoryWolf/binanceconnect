﻿using System;

namespace BinanceModel
{
    public class Kline
    {
        public Kline(DateTime openTime, decimal open, decimal high, decimal low, decimal close, decimal volume, DateTime closeTime, decimal quoteAssetVolume, long numberOfTrades, decimal takerBuyBaseAssetVolume, decimal takerBuyQuoteAssetVolume, decimal ignore)
        {
            OpenTime = openTime;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
            CloseTime = closeTime;
            QuoteAssetVolume = quoteAssetVolume;
            NumberOfTrades = numberOfTrades;
            TakerBuyBaseAssetVolume = takerBuyBaseAssetVolume;
            TakerBuyQuoteAssetVolume = takerBuyQuoteAssetVolume;
            Ignore = ignore;
        }

        public DateTime OpenTime { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get;  }
        public decimal Close { get;  }
        public decimal Volume { get;  }
        public DateTime CloseTime { get;  }
        public decimal QuoteAssetVolume { get;  }
        public long NumberOfTrades { get;  }
        public decimal TakerBuyBaseAssetVolume { get; }
        public decimal TakerBuyQuoteAssetVolume { get; }
        public decimal Ignore { get; }
    }
}
