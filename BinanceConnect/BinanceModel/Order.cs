﻿namespace BinanceModel
{
    public class Order
    {
        public Order(decimal price, decimal quantity)
        {
            Price = price;
            Quantity = quantity;
        }

        public decimal Price { get; }
        public decimal Quantity { get; }
    }
}