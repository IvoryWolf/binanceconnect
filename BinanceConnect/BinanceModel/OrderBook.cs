﻿using System.Collections.Generic;

namespace BinanceModel
{
    public class OrderBook
    {
        public OrderBook(long lastUpdateId, IEnumerable<Order> bids, IEnumerable<Order> asks)
        {
            LastUpdateId = lastUpdateId;
            Bids = bids;
            Asks = asks;
        }

        public long LastUpdateId { get; }
        public IEnumerable<Order> Bids { get; }
        public IEnumerable<Order> Asks { get; }

    }
}
