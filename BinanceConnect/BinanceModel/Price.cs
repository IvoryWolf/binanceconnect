﻿namespace BinanceModel
{
    public class Price
    {
        public Price(string symbol, decimal value)
        {
            Symbol = symbol;
            Value = value;
        }
        public string Symbol { get; }
        public decimal Value { get; }
    }
}
