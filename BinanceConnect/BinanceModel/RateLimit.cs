﻿namespace BinanceModel
{
    public class RateLimit
    {
        public RateLimit(RateLimitType type, RateLimitInterval interval, int intervalValue, int limit)
        {
            Interval = interval;
            IntervalValue = intervalValue;
            Limit = limit;
            Type = type;
        }

        public RateLimitType Type { get; }
        public RateLimitInterval Interval { get; }
        public int IntervalValue { get; }
        public int Limit { get; }

    }
}