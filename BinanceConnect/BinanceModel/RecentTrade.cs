﻿using System;

namespace BinanceModel
{
    public class RecentTrade
    {
        public RecentTrade(long tradeId, decimal price, decimal quantity, decimal quoteQuantity, DateTime time, bool isBuyerMaker, bool isBestMatch)
        {
            TradeId = tradeId;
            Price = price;
            Quantity = quantity;
            QuoteQuantity = quoteQuantity;
            Time = time;
            IsBuyerMaker = isBuyerMaker;
            IsBestMatch = isBestMatch;
        }

        public long TradeId { get; }
        public decimal Price { get; }
        public decimal Quantity { get; }
        public decimal QuoteQuantity { get; }
        public DateTime Time { get; }
        public bool IsBuyerMaker { get; }
        public bool IsBestMatch { get; }

    }
}
