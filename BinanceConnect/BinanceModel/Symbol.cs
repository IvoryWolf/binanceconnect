﻿using System.Collections.Generic;

namespace BinanceModel
{
    public class Symbol
    {
        public Symbol(string ticker, SymbolStatus status, string baseAsset, int baseAssetPrecision, string quoteAsset, int quoteAssetPrecision, int quotePrecision, int baseCommissionPrecision, int quoteCommissionPrecision, IEnumerable<OrderType> orderTypes, bool icebergAllowed, bool ocoAllowed, bool quoteOrderQuantityMarketAllowed, bool isSpotTradingAllowed, bool isMarginTradingAllowed, IEnumerable<Filter> filters, IEnumerable<Permission> permissions)
        {
            Ticker = ticker;
            Status = status;
            BaseAsset = baseAsset;
            BaseAssetPrecision = baseAssetPrecision;
            QuoteAsset = quoteAsset;
            QuoteAssetPrecision = quoteAssetPrecision;
            QuotePrecision = quotePrecision;
            BaseCommissionPrecision = baseCommissionPrecision;
            QuoteCommissionPrecision = quoteCommissionPrecision;
            OrderTypes = orderTypes;
            IcebergAllowed = icebergAllowed;
            OcoAllowed = ocoAllowed;
            QuoteOrderQuantityMarketAllowed = quoteOrderQuantityMarketAllowed;
            IsSpotTradingAllowed = isSpotTradingAllowed;
            IsMarginTradingAllowed = isMarginTradingAllowed;
            Filters = filters;
            Permissions = permissions;
        }

        public string Ticker { get; }
        public SymbolStatus Status { get; }
        public string BaseAsset { get; }
        public int BaseAssetPrecision { get; }
        public string QuoteAsset { get; }
        public int QuoteAssetPrecision { get; }
        public int QuotePrecision { get; }
        public int BaseCommissionPrecision { get; }
        public int QuoteCommissionPrecision { get; }
        public IEnumerable<OrderType> OrderTypes { get; }
        public bool IcebergAllowed { get; }
        public bool OcoAllowed { get; }
        public bool QuoteOrderQuantityMarketAllowed { get; }
        public bool IsSpotTradingAllowed { get; }
        public bool IsMarginTradingAllowed { get; }
        public IEnumerable<Filter> Filters { get; }
        public IEnumerable<Permission> Permissions { get; }
    }
}