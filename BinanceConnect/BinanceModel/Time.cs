﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinanceModel
{
    public class Time
    {
        public Time(DateTime serverTime)
        {
            ServerTime = serverTime;
        }

        public DateTime ServerTime { get; }
    }
}
