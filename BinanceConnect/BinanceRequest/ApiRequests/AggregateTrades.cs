﻿using System;
using BinanceRequest.ApiRequests;
using static BinanceRequest.BinanceDefinitions;
using static BinanceRequest.BinanceUtilities;

namespace BinanceRequest.ApiRequests
{
    public class AggregateTrades : GetRequestBase
    {

        /*
         *
         Response:

            [
              {
                "a": 26129,         // Aggregate tradeId
                "p": "0.01633102",  // Price
                "q": "4.70443515",  // Quantity
                "f": 27781,         // First tradeId
                "l": 27781,         // Last tradeId
                "T": 1498793709153, // Timestamp
                "m": true,          // Was the buyer the maker?
                "M": true           // Was the trade the best price match?
              }
            ]        
         *
         */


        public AggregateTrades(string symbol, int? limit = null)
        {
            Symbol = symbol;
            Limit = limit;
        }

        public AggregateTrades(string symbol, long? fromId, int? limit = null) : this(symbol, limit)
        {
            FromId = fromId;
        }

        public AggregateTrades(string symbol, DateTime startTime, int? limit = null) : this(symbol, limit)
        {
            StartTime = startTime;
        }

        public AggregateTrades(string symbol, DateTime startTime, DateTime endTime, int? limit = null) : this(symbol, startTime, limit)
        {
            EndTime = endTime;
        }

        public string Symbol { get; }
        public long? FromId { get; }
        public DateTime? StartTime { get; }
        public DateTime? EndTime { get; }
        public int? Limit { get; }

        public override string EndPoint => AggregateTradesEndPoint;
        public override string GetQuery()
        {
            if (Query != string.Empty)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";

            if (FromId.HasValue)
            {
                Query += $"&fromId={FromId}";
            }

            if (StartTime != null)
            {
                var startTs = DateToTimeStamp(StartTime.Value.ToUniversalTime());
                long endTs;

                if (EndTime != null)
                {
                    endTs = DateToTimeStamp(EndTime.Value.ToUniversalTime());
                }
                else
                {
                    endTs = startTs + 3600000;  // From experience: both StartTime and EndTime must be posted in the query
                }

                //From the docs: If both startTime and endTime are sent, time between startTime and endTime must be less than 1 hour.
                //From common sense: start time should be before end time.
                if (startTs < endTs && ((endTs - startTs) <= 3600000))
                {
                    Query += $"&startTime={startTs}&endTime={endTs}";
                }

            }

            if (Limit.HasValue & Limit > 0 && Limit <= 1000)
            {
                Query += $"&limit={Limit}";
            }


            return Query;
        }
    }
}
