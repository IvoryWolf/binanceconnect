﻿using BinanceRequest.ApiRequests;

namespace BinanceRequest.ApiRequests
{
    //Current average price for a symbol.
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#current-average-price
    public class AveragePrice : GetRequestBase
    {
        public AveragePrice(string symbol)
        {
            Symbol = symbol;
        }

        public string Symbol { get; }
        public override string EndPoint => BinanceDefinitions.AveragePriceEndPoint;
        public override string GetQuery()
        {
            if (Query != string.Empty)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";

            return Query;
        }
    }
}
