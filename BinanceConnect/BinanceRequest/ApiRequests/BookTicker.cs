﻿using BinanceRequest.ApiRequests;

namespace BinanceRequest.ApiRequests
{
    //Symbol order book ticker
    //Best price/qty on the order book for a symbol or symbols.
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#symbol-order-book-ticker

    public class BookTicker : GetRequestBase
    {
        public BookTicker(string symbol)
        {
            Symbol = symbol;
        }

        public override string EndPoint => BinanceDefinitions.BookTickerEndPoint;

        //if the symbol is not sent, prices for all symbols will be returned in an array.
        public string Symbol { get; }
        public override string GetQuery()
        {
            if (Query != string.Empty || Symbol == null)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";

            return Query;
        }
    }
}
