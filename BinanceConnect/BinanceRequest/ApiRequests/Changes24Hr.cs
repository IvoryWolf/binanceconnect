﻿namespace BinanceRequest.ApiRequests
{
    //24hr ticker price change statistics
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#24hr-ticker-price-change-statistics
    public class Changes24Hr : GetRequestBase
    {
        public Changes24Hr(string symbol = null)
        {
            Symbol = symbol;
        }

        // If the symbol is not sent, tickers for all symbols will be returned in an array.
        public string Symbol { get; }
        public override string EndPoint => BinanceDefinitions.Changes24HrEndPoint;
        public override string GetQuery()
        {
            if (Query != string.Empty || Symbol == null)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";

            return Query;
        }
    }
}
