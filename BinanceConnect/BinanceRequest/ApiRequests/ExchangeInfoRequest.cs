﻿using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#exchange-information
    public class ExchangeInfoRequest : GetRequestBase
    {
        public override string EndPoint => ExchangeEndPoint;
        public override string GetQuery()
        {
            return Query;
        }
    }
}
