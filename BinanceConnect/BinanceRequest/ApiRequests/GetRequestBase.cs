﻿using System.Net.Http;

namespace BinanceRequest.ApiRequests
{
    public abstract class GetRequestBase : RequestBase
    {
        protected string Query  = string.Empty;
        public override HttpMethod HttpMethod => HttpMethod.Get;
    }
}
