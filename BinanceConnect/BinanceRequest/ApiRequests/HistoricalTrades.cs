﻿namespace BinanceRequest.ApiRequests
{
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#old-trade-lookup-market_data
    public class HistoricalTrades : GetRequestBase
    {
        public HistoricalTrades(string symbol, long? fromId, int? limit)
        {
            Symbol = symbol;
            FromId = fromId;
            Limit = limit;
        }

        public HistoricalTrades(string symbol, long? fromId) : this(symbol, fromId, null)
        {
        }

        public HistoricalTrades(string symbol) : this(symbol, null, null)
        {
        }


        public string Symbol { get; }

        public long? FromId { get; }

        public int? Limit { get; }

        public override bool RequiresApiKey() => true;
        
        public override string EndPoint => BinanceDefinitions.HistoricalTradesEndPoint;
        public override string GetQuery()
        {
            if (Query != string.Empty)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";
            if (Limit.HasValue & Limit > 0 && Limit <= 1000)
            {
                Query += $"&limit={Limit}";
            }

            if (FromId.HasValue)
            {
                Query += $"&fromId={FromId}";
            }

            return Query;
        }
    }
}
