﻿using System.Net.Http;

namespace BinanceRequest.ApiRequests
{
    public interface IApiRequest
    {
        HttpRequestMessage GetRequest();

        bool RequiresApiKey();

        bool RequiresSignature();

        void AddHeader(string header, string value);
    }
}
