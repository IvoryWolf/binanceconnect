﻿using System;
using static BinanceRequest.BinanceDefinitions;
using static BinanceRequest.BinanceUtilities;

namespace BinanceRequest.ApiRequests
{
    // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#klinecandlestick-data
    public class Klines : GetRequestBase
    {
        public override string EndPoint => KlinesEndPoint;

        public Klines(string symbol, Interval interval, int? limit)
        {
            Symbol = symbol;
            Interval = interval;
            Limit = limit;
        }

        public Klines(string symbol, Interval interval, DateTime startTime, int? limit) : this(symbol, interval, limit)
        {
            StartTime = startTime;
        }

        public Klines(string symbol, Interval interval, DateTime startTime, DateTime endTime, int? limit) : this(symbol, interval, limit)
        {
            StartTime = startTime;
            EndTime = endTime;
        }

        public string Symbol { get; }
        public Interval Interval { get; }
        public DateTime? StartTime { get; }
        public DateTime? EndTime { get; }
        public int? Limit { get; }

        public override string GetQuery()
        {
            if (Query != string.Empty)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}&interval={Interval.Value}";

            if (StartTime != null)
            {
                var startTs = DateToTimeStamp(StartTime.Value.ToUniversalTime());
                Query += $"&startTime={startTs}";
            }

            if (EndTime != null)
            {
                var endTs = DateToTimeStamp(EndTime.Value.ToUniversalTime());
                Query += $"&endTime={endTs}";
            }

            if (Limit.HasValue & Limit > 0 && Limit <= 1000)
            {
                Query += $"&limit={Limit}";
            }

            return Query;
        }
    }
}
