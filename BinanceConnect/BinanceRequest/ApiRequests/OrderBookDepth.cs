﻿using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#order-book
    public class OrderBookDepth : GetRequestBase
    {
        public OrderBookDepth(string symbol)
        {
            Symbol = symbol;
        }

        public string Symbol { get; }
        public override string EndPoint => OrderBookDepthEndPoint;
        public override string GetQuery()
        {
            return $"?symbol={Symbol}";
        }
    }
}
