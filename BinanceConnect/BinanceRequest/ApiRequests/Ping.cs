﻿using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    public class Ping : GetRequestBase
    {
        //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#test-connectivity
        public override string EndPoint => PingEndPoint;

        public override string GetQuery()
        {
            return Query;
        }
    }
}
