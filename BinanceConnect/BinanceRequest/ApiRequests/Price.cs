﻿namespace BinanceRequest.ApiRequests
{
    //Symbol price ticker
    //Latest price for a symbol or symbols.
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#symbol-price-ticker
    public class Price : GetRequestBase
    {
        public Price(string symbol)
        {
            Symbol = symbol;
        }

        public override string EndPoint => BinanceDefinitions.PriceEndPoint;

        //if the symbol is not sent, prices for all symbols will be returned in an array.
        public string Symbol { get; }
        public override string GetQuery()
        {
            if (Query != string.Empty || Symbol == null)
            {
                return Query;
            }

            Query = $"?symbol={Symbol}";

            return Query;
        }
    }
}
