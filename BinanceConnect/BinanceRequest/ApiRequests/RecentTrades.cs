﻿using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#recent-trades-list
    public class RecentTrades : GetRequestBase
    {
        public RecentTrades(string symbol)
        {
            Symbol = symbol;
        }

        public string Symbol { get; }
        public override string EndPoint => RecentTradesEndPoint;
        public override string GetQuery()
        {
            return $"?symbol={Symbol}";
        }
    }
}
