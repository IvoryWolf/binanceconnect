﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    public abstract class RequestBase : IApiRequest
    {
        private readonly HttpRequestMessage _httpRequestMessage = new HttpRequestMessage();
        private readonly Dictionary<string, string> _headersAndValues = new Dictionary<string, string>();

        public abstract HttpMethod HttpMethod { get; }
        public abstract string EndPoint { get; }

        public virtual bool RequiresApiKey()
        {
            return false;
        }

        public virtual bool RequiresSignature()
        {
            return false;
        }

        public void AddHeader(string header, string value)
        {
            _headersAndValues.Add(header, value);
        }

        public HttpRequestMessage GetRequest()
        {
            _httpRequestMessage.RequestUri = new Uri(GetUri());
            if (!RequiresApiKey()) return _httpRequestMessage;
           
            foreach (var (key, value) in _headersAndValues)
            {
                _httpRequestMessage.Headers.Add(key, value);
            }

            return _httpRequestMessage;
        }


        public string GetUri()
        {
            return $"{BinanceUrl}{EndPoint}{GetQuery()}";
        }

        public abstract string GetQuery();
    }
}
