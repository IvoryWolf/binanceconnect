﻿using System.Net.Http;
using static BinanceRequest.BinanceDefinitions;

namespace BinanceRequest.ApiRequests
{
    // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#check-server-time
    public class Time : GetRequestBase
    {
        public override HttpMethod HttpMethod => HttpMethod.Get;
        public override string EndPoint => TimeEndPoint;
        public override string GetQuery()
        {
            return Query;
        }
    }
}
