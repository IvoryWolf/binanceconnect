﻿namespace BinanceRequest
{
    public static class BinanceDefinitions
    {
        // URLs
        public const string BinanceUrl = "https://api.binance.com";

        // Endpoints
        public const string PingEndPoint = "/api/v3/ping";
        
        public const string TimeEndPoint = "/api/v3/time";

        public const string ExchangeEndPoint = "/api/v3/exchangeInfo";

        public const string OrderBookDepthEndPoint = "/api/v3/depth";

        public const string RecentTradesEndPoint = "/api/v3/trades";

        public const string HistoricalTradesEndPoint = "/api/v3/historicalTrades";

        public const string AggregateTradesEndPoint = "/api/v3/aggTrades";

        public const string KlinesEndPoint = "/api/v3/klines";

        public const string AveragePriceEndPoint = "/api/v3/avgPrice";

        public const string Changes24HrEndPoint = "/api/v3/ticker/24hr";

        public const string PriceEndPoint = "/api/v3/ticker/price";

        public const string BookTickerEndPoint = "/api/v3/ticker/bookTicker";

        // Headers
        public const string HeaderXMbxApikey = "X-MBX-APIKEY";


        // Candlestick Intervals
        // m -> minutes;
        // h -> hours;
        // d -> days;
        // w -> weeks;
        // M -> months
        // Implementation borrowed from https://stackoverflow.com/questions/630803/associating-enums-with-strings-in-c-sharp
        // Making an enum-ish class that gives some type safety and limits values.
        public class Interval
        {
            private Interval(string intervalValue)
            {
                Value = intervalValue;
            }

            public string Value { get; }

            //making these static means they don't appear on the instance, only Value is available.
            public static Interval OneMinute => new Interval("1m");
            public static Interval ThreeMinutes => new Interval("3m");
            public static Interval FiveMinutes => new Interval("5m");
            public static Interval FifteenMinutes => new Interval("15m");
            public static Interval ThirtyMinutes => new Interval("30m");
            public static Interval OneHour => new Interval("1h");
            public static Interval TwoHours => new Interval("2h");
            public static Interval FourHours => new Interval("4h");
            public static Interval SixHours => new Interval("6h");
            public static Interval EightHours => new Interval("8h");
            public static Interval TwelveHours => new Interval("12h");
            public static Interval OneDay => new Interval("1d");
            public static Interval ThreeDays => new Interval("3d");
            public static Interval OneWeek => new Interval("1w");
            public static Interval OneMonth => new Interval("1M");
        }
    }

}
