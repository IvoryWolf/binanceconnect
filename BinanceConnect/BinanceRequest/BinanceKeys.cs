﻿namespace BinanceRequest
{
    public class BinanceKeys
    {
        public string ApiKey { get; set; }
        public string Secret { get; set; }
    }

}
