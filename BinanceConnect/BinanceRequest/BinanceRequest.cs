﻿using System.Net.Http;
using System.Threading.Tasks;
using BinanceRequest.ApiRequests;
using Microsoft.Extensions.Logging;

namespace BinanceRequest
{
    public interface IBinanceRequest
    {
        Task<string> SendRequest(IApiRequest apiRequest);
    }

    public class BinanceRequest : IBinanceRequest
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly BinanceKeys _binanceKeys;

        public BinanceRequest(IHttpClientFactory httpClientFactory, BinanceKeys binanceKeys)
        {
            _binanceKeys = binanceKeys;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<string> SendRequest(IApiRequest apiRequest)
        {
            var client = _httpClientFactory.CreateClient();

            if (apiRequest.RequiresApiKey())
            {
                apiRequest.AddHeader(BinanceDefinitions.HeaderXMbxApikey, _binanceKeys.ApiKey);
            }

            var response = await client.SendAsync(apiRequest.GetRequest());
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return null;
        }
    }
}
