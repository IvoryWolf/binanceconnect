﻿using System;

namespace BinanceRequest
{
    public static class BinanceUtilities
    {
        /// <summary>
        /// Timestamp in milliseconds since 1970. This is require by API calls.
        /// </summary>
        /// <param name="datetime">Date from which the timestamp is required.</param>
        /// <returns></returns>
        public static long DateToTimeStamp(DateTime datetime)
        {
            return new DateTimeOffset(datetime).ToUnixTimeMilliseconds();
        }


        public static DateTime TimeStampToDateTime(long timestamp)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(timestamp).UtcDateTime;
        }
    }
}
