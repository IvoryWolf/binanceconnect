﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BinanceRequest;
using BinanceRequest.ApiRequests;
using FluentAssertions;
using NUnit.Framework;

namespace BinanceConnectTests.ApiRequests
{
    [TestFixture]
    public class ApiRequestTests
    {
        //https://github.com/binance-exchange/binance-official-api-docs

        // Ping
        // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#test-connectivity
        [Test]
        
        public void TestPingRequest()
        {
            var request = new Ping();

            var expectedEndPoint = BinanceDefinitions.PingEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            request.GetQuery().Should().BeEmpty();
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            request.GetUri().Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.RequestUri.AbsoluteUri.Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.Headers.Count().Should().Be(0);

        }

        // ServerTime
        // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#check-server-time

        [Test]
        public void TestTimeRequest()
        {
            var request = new Time();

            var expectedEndPoint = BinanceDefinitions.TimeEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            request.GetQuery().Should().BeEmpty();
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            request.GetUri().Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.RequestUri.AbsoluteUri.Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.Headers.Count().Should().Be(0);

        }

        // Exchange Information
        // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#exchange-information

        [Test]
        public void TestExchangeInfoRequest()
        {
            var request = new ExchangeInfoRequest();

            var expectedEndPoint = BinanceDefinitions.ExchangeEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            request.GetQuery().Should().BeEmpty();
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            request.GetUri().Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.RequestUri.AbsoluteUri.Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint);
            httpRequest.Headers.Count().Should().Be(0);

        }

        // Order book
        //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#order-book

        [Test]
        public void TestOrderBookDepthRequest()
        {
            var symbol = "BTCUSDT";
            var expectedQuery = $"?symbol={symbol}";
            var request = new OrderBookDepth(symbol);

            var expectedEndPoint = BinanceDefinitions.OrderBookDepthEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            request.GetQuery().Should().Be(expectedQuery);
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            request.GetUri().Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint + expectedQuery);
            httpRequest.RequestUri.AbsoluteUri.Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint + expectedQuery);
            httpRequest.Headers.Count().Should().Be(0);

        }

        #region Recent Trades
        // Recent trades
        //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#recent-trades-list

        [Test]
        public void TestRecentTradesRequest()
        {
            var symbol = "BTCUSDT";
            var expectedQuery = $"?symbol={symbol}";
            var request = new RecentTrades(symbol);

            var expectedEndPoint = BinanceDefinitions.RecentTradesEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            request.GetQuery().Should().Be(expectedQuery);
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            request.GetUri().Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint + expectedQuery);
            httpRequest.RequestUri.AbsoluteUri.Should().Be(BinanceDefinitions.BinanceUrl + expectedEndPoint + expectedQuery);
            httpRequest.Headers.Count().Should().Be(0);

        }
        #endregion

        #region Historical Trades
        // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#old-trade-lookup-market_data

        [TestCase("BTCUSDT", null, null, "?symbol=BTCUSDT")]
        [TestCase("", null, null, "?symbol=")] // symbol is mandatory - should always be in query even if we can't  vouch for the value.
        [TestCase("BTCUSDT", 123456, null, "?symbol=BTCUSDT&fromId=123456")]
        [TestCase("BTCUSDT", null, 1000, "?symbol=BTCUSDT&limit=1000")]
        [TestCase("BTCUSDT", 123456, 100, "?symbol=BTCUSDT&limit=100&fromId=123456")]
        [TestCase("BTCUSDT", 123456, 1001, "?symbol=BTCUSDT&fromId=123456")]
        [TestCase("BTCUSDT", 123456, 0, "?symbol=BTCUSDT&fromId=123456")] // limit is out of bounds (1-1000) so do not include
        [TestCase("BTCUSDT", 123456, -0, "?symbol=BTCUSDT&fromId=123456")] // limit is out of bounds (1-1000) so do not include
        public void TestHistoricalTradesRequest(string symbol, long? fromId, int? limit, string expectedQuery)
        {
            var request = new HistoricalTrades(symbol, fromId, limit);
            request.AddHeader("SOME_KIND_OF_HEADER", "a value for the header.");

            var expectedEndPoint = BinanceDefinitions.HistoricalTradesEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            AssertAllQueryPartsMatch(request.GetQuery(), expectedQuery);
            request.RequiresApiKey().Should().BeTrue();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            httpRequest.RequestUri.AbsolutePath.Should().Be(expectedEndPoint);
            AssertAllQueryPartsMatch(httpRequest.RequestUri.Query, expectedQuery);
            httpRequest.Headers.Should().BeEquivalentTo(new KeyValuePair<string, IEnumerable<string>>("SOME_KIND_OF_HEADER", new[] { "a value for the header." }));

        }

        #endregion

        #region Aggregate Trades

        // Recent trades
        // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#old-trade-lookup-market_data

        [TestCase("", null, null, null, null, "?symbol=")] // symbol is mandatory - should always be in query even if we can't vouch for the value.
        [TestCase("BTCUSDT", null, null, null,null,"?symbol=BTCUSDT")]
        [TestCase("BTCUSDT", null, null, null, 1000, "?symbol=BTCUSDT&limit=1000")]

        [TestCase("BTCUSDT", 123456, null, null, null, "?symbol=BTCUSDT&fromId=123456")]
        [TestCase("BTCUSDT", 123456, null, null, 100, "?symbol=BTCUSDT&limit=100&fromId=123456")]

        [TestCase("BTCUSDT", null, "13Sep2019 12:45:43Z", "13Sep2019 13:45:43Z", 100, "?symbol=BTCUSDT&startTime=1568378743000&endTime=1568382343000&limit=100")]
        [TestCase("BTCUSDT", 161528945, "13Sep2019 12:45:43", "13Sep2019 13:45:43", 100, "?symbol=BTCUSDT&fromId=161528945&limit=100")] // From experience frommId cannot be used with startTime and EndTime - we favour fromId is it is sent
        [TestCase("BTCUSDT", null, null, "13Sep2019 13:45:43", 100, "?symbol=BTCUSDT&limit=100")] //StartTime without an EndTime is invalid - we won't add them to the query
        [TestCase("BTCUSDT", null, "13Sep2019 12:45:43", "13Sep2019 13:45:44", null, "?symbol=BTCUSDT")] // EndTime is more than an hour after StartTime: As per docs, this is no valid - we won't add them to the query
        
        [TestCase("BTCUSDT", 123456, null, null, 1001, "?symbol=BTCUSDT&fromId=123456")] // limit is out of bounds (1-1000) so do not include
        [TestCase("BTCUSDT", 123456, null, null, 0, "?symbol=BTCUSDT&fromId=123456")] // limit is out of bounds (1-1000) so do not include
        [TestCase("BTCUSDT", 123456, null, null, -0, "?symbol=BTCUSDT&fromId=123456")] // limit is out of bounds (1-1000) so do not include
        public void TestAggregateTradesRequest(string symbol, long? fromId, DateTime? startTime, DateTime? endTime, int? limit, string expectedQuery)
        {
            AggregateTrades request = new AggregateTrades(symbol, limit);
            if (fromId != null)
            {
                request = new AggregateTrades(symbol, fromId, limit);

            }
            else if (startTime != null)
            {
                if (endTime != null)
                {
                    request = new AggregateTrades(symbol, ((DateTime)startTime), ((DateTime)endTime), limit);
                }
                else
                {
                    request = new AggregateTrades(symbol, (DateTime)startTime, limit);
                }
            }

            var expectedEndPoint = BinanceDefinitions.AggregateTradesEndPoint;
            var httpRequest = request.GetRequest();

            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            AssertAllQueryPartsMatch(request.GetQuery(), expectedQuery);
            request.RequiresApiKey().Should().BeFalse();
            request.RequiresSignature().Should().BeFalse();
            request.EndPoint.Should().Be(expectedEndPoint);
            httpRequest.RequestUri.AbsolutePath.Should().Be(expectedEndPoint);
            AssertAllQueryPartsMatch(httpRequest.RequestUri.Query, expectedQuery);
            httpRequest.Headers.Count().Should().Be(0);

        }

        #endregion

        #region Klines

        //https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#klinecandlestick-data
        [TestCase("BTCUSDT", null, null, -0, "?symbol=BTCUSDT&interval=1d")] // limit is out of bounds (1-1000) so do not include
        [TestCase("BTCUSDT", "13Sep2019Z", null, 100, "?symbol=BTCUSDT&startTime=1568332800000&interval=1d&limit=100")] 
        [TestCase("BTCUSDT", "13Sep2019Z", "15Sep2019Z", 100, "?symbol=BTCUSDT&startTime=1568332800000&endTime=1568505600000&interval=1d&limit=100")]
        public void TestKlinesRequest(string symbol, DateTime? startTime, DateTime? endTime, int? limit, string expectedQuery)
        {
            Klines request;
            if (startTime != null)
            {
                if (endTime != null)
                {
                    request = new Klines(symbol, BinanceDefinitions.Interval.OneDay, (DateTime)startTime, (DateTime)endTime, limit);
                }
                else
                {
                    request = new Klines(symbol, BinanceDefinitions.Interval.OneDay, (DateTime)startTime, limit);                    
                }


            }
            else
            {
                request = new Klines(symbol, BinanceDefinitions.Interval.OneDay, limit);
            }
            
            var expectedEndPoint = BinanceDefinitions.KlinesEndPoint;
            var httpRequest = request.GetRequest();

            AssertRequestIsCorrect(expectedEndPoint, expectedQuery, request, httpRequest);
        }

        #endregion

        #region AveragePrice

        [TestCase("ETHBTC", "?symbol=ETHBTC")]
        public void TestAveragePrice(string symbol, string expectedQuery)
        {
            var request = new AveragePrice(symbol);

            var expectedEndPoint = BinanceDefinitions.AveragePriceEndPoint;
            var httpRequest = request.GetRequest();

            AssertRequestIsCorrect(expectedEndPoint, expectedQuery, request, httpRequest);
        }

        #endregion

        #region Change24Hr

        [TestCase("BTCGBP", "?symbol=BTCGBP")]
        [TestCase(null, "")]
        public void TestChanges24Hr(string symbol, string expectedQuery)
        {
            var request = new Changes24Hr(symbol);

            var expectedEndPoint = BinanceDefinitions.Changes24HrEndPoint;
            var httpRequest = request.GetRequest();

            AssertRequestIsCorrect(expectedEndPoint, expectedQuery, request, httpRequest);
        }

        #endregion

        #region Price

        [TestCase("BTCGBP", "?symbol=BTCGBP")]
        [TestCase(null, "")]
        public void TestPrice(string symbol, string expectedQuery)
        {
            var request = new Price(symbol);

            var expectedEndPoint = BinanceDefinitions.PriceEndPoint;
            var httpRequest = request.GetRequest();

            AssertRequestIsCorrect(expectedEndPoint, expectedQuery, request, httpRequest);
        }

        #endregion

        #region BookTicker

        [TestCase("BTCGBP", "?symbol=BTCGBP")]
        [TestCase(null, "")]
        public void TestBookTicker(string symbol, string expectedQuery)
        {
            var request = new BookTicker(symbol);

            var expectedEndPoint = BinanceDefinitions.BookTickerEndPoint;
            var httpRequest = request.GetRequest();

            AssertRequestIsCorrect(expectedEndPoint, expectedQuery, request, httpRequest);
        }

        #endregion

        private void AssertRequestIsCorrect(string expectedEndPoint, string expectedQuery, RequestBase request, HttpRequestMessage httpRequest, bool requiresApiKey = false, bool requiresSignature = false)
        {
            Console.WriteLine(httpRequest.RequestUri);

            request.HttpMethod.Should().Be(HttpMethod.Get);
            AssertAllQueryPartsMatch(request.GetQuery(), expectedQuery);
            request.RequiresApiKey().Should().Be(requiresApiKey);
            request.RequiresSignature().Should().Be(requiresSignature);
            request.EndPoint.Should().Be(expectedEndPoint);
            httpRequest.RequestUri.AbsolutePath.Should().Be(expectedEndPoint);
            AssertAllQueryPartsMatch(httpRequest.RequestUri.Query, expectedQuery);
            httpRequest.Headers.Count().Should().Be(0);
        }

        private void AssertAllQueryPartsMatch(string queryUnderTest, string expectedQuery)
        {
            queryUnderTest.Split("&").Should().BeEquivalentTo(expectedQuery.Split("&"));
        }
    }


}

