﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BinanceRequest.ApiRequests;
using FluentAssertions;
using NUnit.Framework;

namespace BinanceConnectTests.ApiRequests
{
    [TestFixture]
    public class RequestBaseTests
    {
        [Test]
        public void WhenRequiresApiKeyTrueAndHeaderAddedThenRequestContainsHeaders()
        {
            var sut = new ConcreteRequestBaseRequiresApiKey();
            sut.AddHeader("THIS_HEADER", "that value");

            var request = sut.GetRequest();
            request.Headers.Contains("THIS_HEADER").Should().BeTrue();
            request.Headers.Should().BeEquivalentTo(new KeyValuePair<string, IEnumerable<string>>("THIS_HEADER", new [] {"that value"}));
        }

        [Test]
        public void WhenRequiresApiKeyFalseAndHeaderAddedThenRequestDoesNotContainsHeaders()
        {
            var sut = new ConcreteRequestBaseDoesNotRequireApiKey();
            sut.AddHeader("THIS_HEADER", "that value");

            var request = sut.GetRequest();
            request.Headers.Count().Should().Be(0);
        }


        // We are testing functionality on an abstract class so create some concrete classes to test with - one for each scenario.
        private class ConcreteRequestBaseRequiresApiKey : RequestBase
        {
            public override HttpMethod HttpMethod => HttpMethod.Get;
            public override string EndPoint => "/some/path";
            public override string GetQuery()
            {
                return string.Empty;
            }

            public override bool RequiresApiKey()
            {
                return true;
            }
        }

        private class ConcreteRequestBaseDoesNotRequireApiKey : RequestBase
        {
            public override HttpMethod HttpMethod => HttpMethod.Get;
            public override string EndPoint => "/some/path";
            public override string GetQuery()
            {
                return string.Empty;
            }

            public override bool RequiresApiKey()
            {
                return false;
            }
        }
    }
}
