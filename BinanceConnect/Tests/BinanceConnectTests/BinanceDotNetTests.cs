﻿using System;
using BinanceRequest;
using FluentAssertions;
using NUnit.Framework;

namespace BinanceConnectTests
{
    [TestFixture]
    public class BinanceDotNetTests
    {

        [TestCase("02Jan1970", 86400000)]
        [TestCase("01Aug2020 12:23:43Z", 1596284623000)]
        public void TestTimestampSecondsSince1970(string datetime, long expected)
        {
            var testDate = DateTime.Parse(datetime).ToUniversalTime();
            var ts = BinanceUtilities.DateToTimeStamp(testDate);
            ts.Should().Be(expected);
        }

        [TestCase(86400000, "02Jan1970")]
        [TestCase(1596284623000, "01Aug2020 12:23:43Z")]
        public void TestTimestampConversion(long timestamp, string expected)
        {
            var expectedDateTime = DateTime.Parse(expected).ToUniversalTime();
            var convertedDateTime = BinanceUtilities.TimeStampToDateTime(timestamp).ToUniversalTime();
            convertedDateTime.Should().Be(expectedDateTime);
        }
    }
}
