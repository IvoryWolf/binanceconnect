﻿using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using BinanceDotNetApi;
using BinanceModel;
using BinanceRequest;
using BinanceRequest.ApiRequests;
using NSubstitute;
using NUnit.Framework;

namespace BinanceDotNetApiTests
{
    [TestFixture]
    public class BinanceDotNetTests
    {
        [Test]
        public void ExchangeInfoTest()
        {
            var binanceRequest = Substitute.For<IBinanceRequest>();
            var mapper = Substitute.For<IMapper>();
            var sut = new BinanceDotNet(binanceRequest, mapper);

            binanceRequest.SendRequest(Arg.Any<IApiRequest>()).Returns(Task.FromResult("{}"));
            _ =  sut.ExchangeInfo().Result;

            binanceRequest.Received(1).SendRequest(Arg.Any<ExchangeInfoRequest>());
            mapper.Received(1).Map<ExchangeInfo>(Arg.Any<JsonDocument>());
        }

    }
}
