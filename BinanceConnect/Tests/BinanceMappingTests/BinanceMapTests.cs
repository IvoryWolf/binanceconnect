﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using AutoMapper;
using BinanceMapping;
using BinanceModel;
using FluentAssertions;
using NUnit.Framework;
using static BinanceMappingTests.Data;
using static BinanceRequest.BinanceUtilities;
using ExchangeInfo = BinanceModel.ExchangeInfo;
using Mapper = AutoMapper.Mapper;

namespace BinanceMappingTests
{
    [TestFixture]
    public class BinanceMapTests
    {
        [Test]
        public void TestKlinesMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Klines);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());
            
            var mapper = new Mapper(config);

            var sut = mapper.Map<List<Kline>>(jsonDoc);

            // check the values against the simple deserialisation of the raw data.
            // the kline data is an array of unnamed data. The KlineFields enum represents the data in each position.
            var data = JsonSerializer.Deserialize<List<object[]>>(Klines);

            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateArray().First().EnumerateArray().Count());

            sut[0].Close.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int) KlineFields.Close].ToString());
            sut[0].High.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.High].ToString());
            DateToTimeStamp(sut[0].CloseTime).Should().Be(((JsonElement)data[0][(int)KlineFields.CloseTime]).GetInt64());
            DateToTimeStamp(sut[0].OpenTime).Should().Be(((JsonElement)data[0][(int)KlineFields.OpenTime]).GetInt64());
            sut[0].QuoteAssetVolume.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.QuoteAssetVolume].ToString());
            sut[0].NumberOfTrades.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.NumberOfTrades].ToString());
            sut[0].TakerBuyBaseAssetVolume.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.TakerBuyBaseAssetVolume].ToString());
            sut[0].Ignore.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.Ignore].ToString());
            sut[0].Low.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.Low].ToString());
            sut[0].Open.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.Open].ToString());
            sut[0].Volume.ToString(CultureInfo.InvariantCulture).Should().Be(data[0][(int)KlineFields.Volume].ToString());
            
        }

        [Test]
        public void TestPriceMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Prices);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<Price>>(jsonDoc);
            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateArray().First().EnumerateObject().Count());

            sut[0].Symbol.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("symbol").ToString());
            sut[0].Value.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("price").ToString());

        }

        [Test]
        public void TestAggregateTradeMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(AggregateTrades);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<AggregateTrade>>(jsonDoc);
            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateArray().First().EnumerateObject().Count());

            sut[0].TradeId.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("a").ToString());
            sut[0].FirstTradeId.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("f").ToString());
            sut[0].Quantity.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("q").ToString());
            sut[0].BuyerWasTheMaker.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("m").ToString());
            sut[0].LastTradeId.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("l").ToString());
            sut[0].Price.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("p").ToString());
            DateToTimeStamp(sut[0].TimeStamp).ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("T").ToString());
            sut[0].TradeWasBestPriceMatch.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.EnumerateArray().First().GetProperty("M").ToString());
        }

        [Test]
        public void TestTimeMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(ServerTime);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<Time>(jsonDoc);
            // check every property has a field in the json
            sut.GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());

            DateToTimeStamp(sut.ServerTime).ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("serverTime").GetInt64().ToString());

        }

        [Test]
        public void TestAveragePriceMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(CurrentAveragePrice);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<AveragePrice>(jsonDoc);

            // check every property has a field in the json
            sut.GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());

            sut.Minutes.Should().Be(jsonDoc.RootElement.GetProperty("mins").GetInt32());
            sut.Price.ToString().Should().Be(jsonDoc.RootElement.GetProperty("price").GetString());

        }

        [Test]
        public void TestBookTickerSingleMapping()
        {
            //The JSON returned in this query will either be an array or a single element depending on if a symbol was specified.
            //This is the test case for when a symbols has been specified - a single object will be returned.
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(BookTickerSingle);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<BookTicker>>(jsonDoc);

            sut.Count.Should().Be(1);
            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());

            sut.First().Symbol.Should().Be(jsonDoc.RootElement.GetProperty("symbol").GetString());
            sut.First().AskPrice.ToString().Should().Be(jsonDoc.RootElement.GetProperty("askPrice").GetString());
            sut.First().AskQuantity.ToString().Should().Be(jsonDoc.RootElement.GetProperty("askQty").GetString());
            sut.First().BidPrice.ToString().Should().Be(jsonDoc.RootElement.GetProperty("bidPrice").GetString());
            sut.First().BidQuantity.ToString().Should().Be(jsonDoc.RootElement.GetProperty("bidQty").GetString());

        }

        [Test]
        public void TestBookTickerMultipleMapping()
        {
            //The JSON returned in this query will either be an array or a single element depending on if a symbol was specified.
            //This is the test case for when a symbols has been NOT specified - a and ARRAY of objects will be returned.
            var jsonDoc = JsonDocument.Parse(BookTickerMultiple);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<BookTicker>>(jsonDoc);

            sut.Count.Should().Be(3);
            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateArray().First().EnumerateObject().Count());

            sut.Last().Symbol.Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("symbol").GetString());
            sut.Last().AskPrice.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("askPrice").GetString());
            sut.Last().AskQuantity.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("askQty").GetString());
            sut.Last().BidPrice.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("bidPrice").GetString());
            sut.Last().BidQuantity.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("bidQty").GetString());
        }

        [Test]
        public void TestChange24HrMapping()
        {
            var jsonDoc = JsonDocument.Parse(Data.Changes24Hr);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<Changes24Hr>(jsonDoc);

            // check every property has a field in the json
            sut.GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());

            sut.Symbol.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("symbol").ToString());
            sut.PriceChange.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("priceChange").ToString());
            sut.PriceChangePercent.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("priceChangePercent").ToString());
            sut.WeightedAveragePrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("weightedAvgPrice").ToString());
            sut.PreviousClosePrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("prevClosePrice").ToString());
            sut.LastPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("lastPrice").ToString());
            sut.LastQuantity.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("lastQty").ToString());
            sut.BidPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("bidPrice").ToString());
            sut.AskPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("askPrice").ToString());
            sut.AskQuantity.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("askQty").ToString());
            sut.OpenPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("openPrice").ToString());
            sut.HighPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("highPrice").ToString());
            sut.LowPrice.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("lowPrice").ToString());
            sut.Volume.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("volume").ToString());
            sut.QuoteVolume.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("quoteVolume").ToString());
            DateToTimeStamp(sut.OpenTime).Should().Be(jsonDoc.RootElement.GetProperty("openTime").GetInt64());
            DateToTimeStamp(sut.CloseTime).Should().Be(jsonDoc.RootElement.GetProperty("closeTime").GetInt64());
            sut.FirstTradeId.Should().Be(jsonDoc.RootElement.GetProperty("firstId").GetInt64());
            sut.LastTradeId.Should().Be(jsonDoc.RootElement.GetProperty("lastId").GetInt64());
            sut.Count.Should().Be(jsonDoc.RootElement.GetProperty("count").GetInt64());

        }

        [Test]
        public void TestHistoricalTradeMapping()
        {
            var jsonDoc = JsonDocument.Parse(HistoricalTrades);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<HistoricalTrade>>(jsonDoc);

            sut.Count.Should().Be(3);
            // check every property has a field in the json
            sut[0].GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateArray().First().EnumerateObject().Count());

            sut.Last().TradeId.Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("id").GetInt64());
            sut.Last().Price.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("price").GetString());
            sut.Last().Quantity.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("qty").GetString());
            sut.Last().QuoteQuantity.ToString().Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("quoteQty").GetString());
            DateToTimeStamp(sut.Last().Time).Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("time").GetInt64());
            sut.Last().IsBuyerMaker.Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("isBuyerMaker").GetBoolean());
            sut.Last().IsBestMatch.Should().Be(jsonDoc.RootElement.EnumerateArray().Last().GetProperty("isBestMatch").GetBoolean());

        }

        [Test]
        public void TestOrderBookMapping()
        {
            var jsonDoc = JsonDocument.Parse(Data.OrderBook);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<OrderBook>(jsonDoc);

            sut.LastUpdateId.Should().Be(jsonDoc.RootElement.GetProperty("lastUpdateId").GetInt64());
            sut.Bids.Count().Should().Be(5);
            sut.Asks.Count().Should().Be(5);
            // check every property has a field in the json
            sut.GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());
            // Just check the first bid price and last ask quantity
            sut.Bids.First().Price.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("bids").EnumerateArray().First().EnumerateArray().First().GetString());
            sut.Asks.Last().Quantity.ToString(CultureInfo.InvariantCulture).Should().Be(jsonDoc.RootElement.GetProperty("asks").EnumerateArray().Last().EnumerateArray().Last().GetString());

        }

        [Test]
        public void TestRecentTradeMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(RecentTrades);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<RecentTrade>>(jsonDoc);

            var rootArray = jsonDoc.RootElement.EnumerateArray();

            sut.Count.Should().Be(3);

            // check every property has a field in the json
            sut.GetType().GetProperties().Length.Should().Be(rootArray.Count());

            sut[0].TradeId.Should().Be(rootArray.First().GetProperty("id").GetInt64());
            sut[2].Price.ToString().Should().Be(rootArray.Last().GetProperty("price").GetString());
            sut[0].Quantity.ToString().Should().Be(rootArray.First().GetProperty("qty").GetString());
            sut[2].QuoteQuantity.ToString().Should().Be(rootArray.Last().GetProperty("quoteQty").GetString());
            DateToTimeStamp(sut[0].Time).Should().Be(rootArray.First().GetProperty("time").GetInt64());
            sut[2].IsBestMatch.Should().Be(rootArray.First().GetProperty("isBuyerMaker").GetBoolean());
            sut[0].IsBestMatch.Should().Be(rootArray.First().GetProperty("isBestMatch").GetBoolean());
        }

        #region ExchangeInfo

        // There are a few components to a full ExchangeInfo (RateLimits, Symbols, etc) mapping. The sub components
        // will be tested separately.

        [Test]
        public void TestRateLimitMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Data.ExchangeInfo);
            var rateLimits = jsonDoc.RootElement.GetProperty("rateLimits");

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<RateLimit>>(rateLimits);


            // check every property has a field in the json (Exchange Info)
            sut.First().GetType().GetProperties().Length.Should().Be(rateLimits.EnumerateArray().First().EnumerateObject().Count());

            sut.First().Type.Should().Be(RateLimitType.RequestWeight);
            sut.Last().Interval.Should().Be(RateLimitInterval.Day);
            sut.First().IntervalValue.Should().Be(rateLimits.EnumerateArray().First().GetProperty("intervalNum").GetInt32());
            sut.Last().Limit.Should().Be(rateLimits.EnumerateArray().Last().GetProperty("limit").GetInt32());
        }


        [Test]
        public void TestFilterMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Data.ExchangeInfo);
            var exchangeFilters = jsonDoc.RootElement.GetProperty("exchangeFilters");

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<Filter>>(exchangeFilters);


            // check every property has a field in the json (Exchange Info)
            sut.First().GetType().GetProperties().Length.Should().Be(exchangeFilters.EnumerateArray().First().EnumerateObject().Count());

            sut.First().FilterType.Should().Be(FilterType.ExchangeMaxNumOrders);
            sut.Last().FilterType.Should().Be(FilterType.ExchangeMaxAlgoOrders);
            sut.First().Fields["maxNumOrders"].Should().Be(exchangeFilters.EnumerateArray().First().GetProperty("maxNumOrders").GetInt32());
            sut.Last().Fields["maxNumAlgoOrders"].Should().Be(exchangeFilters.EnumerateArray().Last().GetProperty("maxNumAlgoOrders").GetInt32());
        }

        [Test]
        public void TestSymbolMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Data.ExchangeInfo);
            var symbols = jsonDoc.RootElement.GetProperty("symbols");

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<List<Symbol>>(symbols);
            
            // check every property has a field in the json (Exchange Info)
            sut.First().GetType().GetProperties().Length.Should().Be(symbols.EnumerateArray().First().EnumerateObject().Count());

            sut.First().Ticker.Should().Be(symbols.EnumerateArray().First().GetProperty("symbol").GetString());
            sut.First().BaseAsset.Should().Be(symbols.EnumerateArray().First().GetProperty("baseAsset").GetString());
            sut.First().BaseAssetPrecision.Should().Be(symbols.EnumerateArray().First().GetProperty("baseAssetPrecision").GetInt32());
            sut.First().BaseCommissionPrecision.Should().Be(symbols.EnumerateArray().First().GetProperty("baseCommissionPrecision").GetInt32());
            sut.First().IcebergAllowed.Should().Be(symbols.EnumerateArray().First().GetProperty("icebergAllowed").GetBoolean());
            sut.First().OcoAllowed.Should().Be(symbols.EnumerateArray().First().GetProperty("ocoAllowed").GetBoolean());
            sut.First().QuoteOrderQuantityMarketAllowed.Should().Be(symbols.EnumerateArray().First().GetProperty("quoteOrderQtyMarketAllowed").GetBoolean());
            sut.First().IsSpotTradingAllowed.Should().Be(symbols.EnumerateArray().First().GetProperty("isSpotTradingAllowed").GetBoolean());
            sut.First().IsMarginTradingAllowed.Should().Be(symbols.EnumerateArray().First().GetProperty("isMarginTradingAllowed").GetBoolean());
            sut.First().Filters.Count().Should().Be(8);
            sut.First().OrderTypes.Count().Should().Be(5);
            sut.First().Permissions.Count().Should().Be(2);
            sut.First().OrderTypes.ElementAt(2).Should().Be(OrderType.Market);
            sut.First().Permissions.ElementAt(1).Should().Be(Permission.Margin);
        }

        [Test]
        public void TestExchangeInfoMapping()
        {
            // Conversion is from a JsonDoc object so create one.
            var jsonDoc = JsonDocument.Parse(Data.ExchangeInfo);

            var config = new MapperConfiguration(cfg => cfg.AddProfile<BinanceProfile>());

            var mapper = new Mapper(config);

            var sut = mapper.Map<ExchangeInfo>(jsonDoc);


            // check every property has a field in the json (Exchange Info)
            sut.GetType().GetProperties().Length.Should().Be(jsonDoc.RootElement.EnumerateObject().Count());

            sut.RateLimits.Count().Should().Be(3);
            sut.ExchangeFilters.Count().Should().Be(2);
            sut.Symbols.Count().Should().Be(3);

            sut.Timezone.Should().Be(jsonDoc.RootElement.GetProperty("timezone").GetString());
            DateToTimeStamp(sut.ServerTime).Should().Be(jsonDoc.RootElement.GetProperty("serverTime").GetInt64());
        }

        #endregion

    }
}
