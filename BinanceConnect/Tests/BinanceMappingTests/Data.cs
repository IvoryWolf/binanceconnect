﻿namespace BinanceMappingTests
{
    public static class Data
    {
        public const string Klines = @"
        [
          [
            1568332800000,
            ""10415.01000000"",
            ""10439.00000000"",
            ""10153.00000000"",
            ""10342.06000000"",
            ""30280.33977600"",
            1568419199999,
            ""311632094.25618850"",
            304947,
            ""16763.85466300"",
            ""172561765.65306618"",
            ""0""
          ],
          [
	        1568419200000,
	        ""10344.13000000"",
	        ""10419.99000000"",
	        ""10222.33000000"",
	        ""10335.02000000"",
	        ""23621.53351900"",
	        1568505599999,
	        ""244063007.09022474"",
	        253929,
	        ""13495.98669100"",
	        ""139491411.26572924"",
	        ""0""
          ],
          [
	        1568505600000,
	        ""10332.81000000"",
	        ""10360.00000000"",
	        ""10252.15000000"",
	        ""10302.01000000"",
	        ""18047.65401300"",
	        1568591999999,
	        ""185853330.43704256"",
	        194733,
	        ""8221.80305500"",
	        ""84690636.88555922"",
	        ""0""
          ]
        ]";

        public const string Prices = @"
        [
          {
            ""symbol"": ""ETHBTC"",
            ""price"": ""0.03376700""
          },
          {
	        ""symbol"": ""LTCBTC"",
            ""price"": ""0.00469800""
          },
          {
	        ""symbol"": ""BNBBTC"",
            ""price"": ""0.00227000""
          },
          {
	        ""symbol"": ""NEOBTC"",
            ""price"": ""0.00169500""
          }
        ]";

        public const string AggregateTrades = @"
	    [
	      {
	        ""a"": 161528945,
	        ""p"": ""10308.86000000"",
	        ""q"": ""0.03113500"",
	        ""f"": 178810354,
	        ""l"": 178810354,
	        ""T"": 1568378723517,
	        ""m"": false,
	        ""M"": true
	      },
	      {
		    ""a"": 161528946,
	        ""p"": ""10307.51000000"",
	        ""q"": ""0.06003100"",
	        ""f"": 178810355,
	        ""l"": 178810355,
	        ""T"": 1568378723650,
	        ""m"": true,
	        ""M"": true
	      },
	      {
		    ""a"": 161528947,
	        ""p"": ""10308.82000000"",
	        ""q"": ""0.07153100"",
	        ""f"": 178810356,
	        ""l"": 178810356,
	        ""T"": 1568378724243,
	        ""m"": false,
	        ""M"": true
	      }]";

        public const string ServerTime = @"
        {
          ""serverTime"": 1600879367941
            }        
        ";

        public const string CurrentAveragePrice = @"
        {
            ""mins"": 5,
            ""price"": ""0.03282794""
        }";

        public const string BookTickerSingle = @"
        {
            ""symbol"": ""BTCGBP"",
            ""bidPrice"": ""8394.56000000"",
            ""bidQty"": ""0.00463700"",
            ""askPrice"": ""8396.65000000"",
            ""askQty"": ""0.00353000""
        }";

        public const string BookTickerMultiple = @"
        [
            {
                ""symbol"": ""ETHBTC"",
                ""bidPrice"": ""0.03241300"",
                ""bidQty"": ""2.17600000"",
                ""askPrice"": ""0.03241400"",
                ""askQty"": ""66.50000000""
            },
            {
                ""symbol"": ""LTCBTC"",
                ""bidPrice"": ""0.00421400"",
                ""bidQty"": ""29.37000000"",
                ""askPrice"": ""0.00421500"",
                ""askQty"": ""0.27000000""
            },
            {
                ""symbol"": ""BNBBTC"",
                ""bidPrice"": ""0.00227450"",
                ""bidQty"": ""1.52000000"",
                ""askPrice"": ""0.00227480"",
                ""askQty"": ""1.00000000""
            }
        ]";

        public const string Changes24Hr = @"
        {
            ""symbol"": ""BTCGBP"",
            ""priceChange"": ""306.79000000"",
            ""priceChangePercent"": ""3.788"",
            ""weightedAvgPrice"": ""8255.58814393"",
            ""prevClosePrice"": ""8113.38000000"",
            ""lastPrice"": ""8406.75000000"",
            ""lastQty"": ""0.14236800"",
            ""bidPrice"": ""8390.24000000"",
            ""bidQty"": ""0.25009000"",
            ""askPrice"": ""8407.22000000"",
            ""askQty"": ""0.00160800"",
            ""openPrice"": ""8099.96000000"",
            ""highPrice"": ""8444.48000000"",
            ""lowPrice"": ""8058.46000000"",
            ""volume"": ""90.07809200"",
            ""quoteVolume"": ""743647.62834344"",
            ""openTime"": 1600895135748,
            ""closeTime"": 1600981535748,
            ""firstId"": 203238,
            ""lastId"": 206492,
            ""count"": 3255
        }";

        public const string HistoricalTrades = @"
        [
            {
                ""id"":123456,
                ""price"":""4332.89000000"",
                ""qty"":""0.02736700"",
                ""quoteQty"":""118.57820063"",
                ""time"":1504942423422,
                ""isBuyerMaker"":true,
                ""isBestMatch"":true
            },
            {
                ""id"":123457,
                ""price"":""4343.37000000"",
                ""qty"":""0.24585700"",
                ""quoteQty"":""1067.84791809"",
                ""time"":1504942440116,
                ""isBuyerMaker"":true,
                ""isBestMatch"":true
            },
            {
                ""id"":123458,
                ""price"":""4332.89000000"",
                ""qty"":""0.04000000"",
                ""quoteQty"":""173.31560000"",
                ""time"":1504942440426,
                ""isBuyerMaker"":true,
                ""isBestMatch"":true
            }
        ]";

        public const string OrderBook = @"
        {
          ""lastUpdateId"": 6051946837,
          ""bids"": [
            [
              ""10762.51000000"",
              ""0.00000100""
            ],
            [
              ""10762.50000000"",
              ""0.01500000""
            ],
            [
              ""10762.27000000"",
              ""0.32129200""
            ],
            [
              ""10762.26000000"",
              ""0.01724600""
            ],
            [
              ""10762.06000000"",
              ""0.08405800""
            ]
          ],
          ""asks"": [
            [
              ""10762.52000000"",
              ""8.24593300""
            ],
            [
              ""10762.55000000"",
              ""0.02000000""
            ],
            [
              ""10762.64000000"",
              ""0.01000000""
            ],
            [
              ""10762.76000000"",
              ""0.08932200""
            ],
            [
              ""10762.87000000"",
              ""0.01724600""
            ]
          ]
        }";

        public const string RecentTrades = @"
        [
          {
            ""id"": 430187953,
            ""price"": ""10616.38000000"",
            ""qty"": ""0.06027700"",
            ""quoteQty"": ""639.92353726"",
            ""time"": 1602159348648,
            ""isBuyerMaker"": true,
            ""isBestMatch"": true
          },
          {
            ""id"": 430187954,
            ""price"": ""10616.38000000"",
            ""qty"": ""0.30000000"",
            ""quoteQty"": ""3184.91400000"",
            ""time"": 1602159348746,
            ""isBuyerMaker"": true,
            ""isBestMatch"": true
          },
          {
            ""id"": 430187955,
            ""price"": ""10616.39000000"",
            ""qty"": ""0.00190900"",
            ""quoteQty"": ""20.26668851"",
            ""time"": 1602159348886,
            ""isBuyerMaker"": false,
            ""isBestMatch"": true
          }
        ]";

        public const string ExchangeInfo = @"
        {
          ""timezone"": ""UTC"",
          ""serverTime"": 1602174766972,
          ""rateLimits"": [
            {
              ""rateLimitType"": ""REQUEST_WEIGHT"",
              ""interval"": ""MINUTE"",
              ""intervalNum"": 1,
              ""limit"": 1200
            },
            {
              ""rateLimitType"": ""ORDERS"",
              ""interval"": ""SECOND"",
              ""intervalNum"": 10,
              ""limit"": 100
            },
            {
              ""rateLimitType"": ""ORDERS"",
              ""interval"": ""DAY"",
              ""intervalNum"": 1,
              ""limit"": 200000
            }
          ],
          ""exchangeFilters"": [
            {
              ""filterType"": ""EXCHANGE_MAX_NUM_ORDERS"",
              ""maxNumOrders"": 1000
            },
            {
              ""filterType"": ""EXCHANGE_MAX_ALGO_ORDERS"",
              ""maxNumAlgoOrders"": 200
            }
          ],
          ""symbols"": [
            {
              ""symbol"": ""ETHBTC"",
              ""status"": ""TRADING"",
              ""baseAsset"": ""ETH"",
              ""baseAssetPrecision"": 8,
              ""quoteAsset"": ""BTC"",
              ""quotePrecision"": 8,
              ""quoteAssetPrecision"": 8,
              ""baseCommissionPrecision"": 8,
              ""quoteCommissionPrecision"": 8,
              ""orderTypes"": [
                ""LIMIT"",
                ""LIMIT_MAKER"",
                ""MARKET"",
                ""STOP_LOSS_LIMIT"",
                ""TAKE_PROFIT_LIMIT""
              ],
              ""icebergAllowed"": true,
              ""ocoAllowed"": true,
              ""quoteOrderQtyMarketAllowed"": true,
              ""isSpotTradingAllowed"": true,
              ""isMarginTradingAllowed"": true,
              ""filters"": [
                {
                  ""filterType"": ""PRICE_FILTER"",
                  ""minPrice"": ""0.00000100"",
                  ""maxPrice"": ""100000.00000000"",
                  ""tickSize"": ""0.00000100""
                },
                {
                  ""filterType"": ""PERCENT_PRICE"",
                  ""multiplierUp"": ""5"",
                  ""multiplierDown"": ""0.2"",
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""LOT_SIZE"",
                  ""minQty"": ""0.00100000"",
                  ""maxQty"": ""100000.00000000"",
                  ""stepSize"": ""0.00100000""
                },
                {
                  ""filterType"": ""MIN_NOTIONAL"",
                  ""minNotional"": ""0.00010000"",
                  ""applyToMarket"": true,
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""ICEBERG_PARTS"",
                  ""limit"": 10
                },
                {
                  ""filterType"": ""MARKET_LOT_SIZE"",
                  ""minQty"": ""0.00000000"",
                  ""maxQty"": ""6469.82632639"",
                  ""stepSize"": ""0.00000000""
                },
                {
                  ""filterType"": ""MAX_NUM_ALGO_ORDERS"",
                  ""maxNumAlgoOrders"": 5
                },
                {
                  ""filterType"": ""MAX_NUM_ORDERS"",
                  ""maxNumOrders"": 200
                }
              ],
              ""permissions"": [
                ""SPOT"",
                ""MARGIN""
              ]
            },
            {
              ""symbol"": ""LTCBTC"",
              ""status"": ""TRADING"",
              ""baseAsset"": ""LTC"",
              ""baseAssetPrecision"": 8,
              ""quoteAsset"": ""BTC"",
              ""quotePrecision"": 8,
              ""quoteAssetPrecision"": 8,
              ""baseCommissionPrecision"": 8,
              ""quoteCommissionPrecision"": 8,
              ""orderTypes"": [
                ""LIMIT"",
                ""LIMIT_MAKER"",
                ""MARKET"",
                ""STOP_LOSS_LIMIT"",
                ""TAKE_PROFIT_LIMIT""
              ],
              ""icebergAllowed"": true,
              ""ocoAllowed"": true,
              ""quoteOrderQtyMarketAllowed"": true,
              ""isSpotTradingAllowed"": true,
              ""isMarginTradingAllowed"": true,
              ""filters"": [
                {
                  ""filterType"": ""PRICE_FILTER"",
                  ""minPrice"": ""0.00000100"",
                  ""maxPrice"": ""100000.00000000"",
                  ""tickSize"": ""0.00000100""
                },
                {
                  ""filterType"": ""PERCENT_PRICE"",
                  ""multiplierUp"": ""5"",
                  ""multiplierDown"": ""0.2"",
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""LOT_SIZE"",
                  ""minQty"": ""0.01000000"",
                  ""maxQty"": ""100000.00000000"",
                  ""stepSize"": ""0.01000000""
                },
                {
                  ""filterType"": ""MIN_NOTIONAL"",
                  ""minNotional"": ""0.00010000"",
                  ""applyToMarket"": true,
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""ICEBERG_PARTS"",
                  ""limit"": 10
                },
                {
                  ""filterType"": ""MARKET_LOT_SIZE"",
                  ""minQty"": ""0.00000000"",
                  ""maxQty"": ""27298.06077832"",
                  ""stepSize"": ""0.00000000""
                },
                {
                  ""filterType"": ""MAX_NUM_ALGO_ORDERS"",
                  ""maxNumAlgoOrders"": 5
                },
                {
                  ""filterType"": ""MAX_NUM_ORDERS"",
                  ""maxNumOrders"": 200
                }
              ],
              ""permissions"": [
                ""SPOT"",
                ""MARGIN""
              ]
            },
            {
              ""symbol"": ""BNBBTC"",
              ""status"": ""TRADING"",
              ""baseAsset"": ""BNB"",
              ""baseAssetPrecision"": 8,
              ""quoteAsset"": ""BTC"",
              ""quotePrecision"": 8,
              ""quoteAssetPrecision"": 8,
              ""baseCommissionPrecision"": 8,
              ""quoteCommissionPrecision"": 8,
              ""orderTypes"": [
                ""LIMIT"",
                ""LIMIT_MAKER"",
                ""MARKET"",
                ""STOP_LOSS_LIMIT"",
                ""TAKE_PROFIT_LIMIT""
              ],
              ""icebergAllowed"": true,
              ""ocoAllowed"": true,
              ""quoteOrderQtyMarketAllowed"": true,
              ""isSpotTradingAllowed"": true,
              ""isMarginTradingAllowed"": true,
              ""filters"": [
                {
                  ""filterType"": ""PRICE_FILTER"",
                  ""minPrice"": ""0.00000010"",
                  ""maxPrice"": ""100000.00000000"",
                  ""tickSize"": ""0.00000010""
                },
                {
                  ""filterType"": ""PERCENT_PRICE"",
                  ""multiplierUp"": ""5"",
                  ""multiplierDown"": ""0.2"",
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""LOT_SIZE"",
                  ""minQty"": ""0.01000000"",
                  ""maxQty"": ""100000.00000000"",
                  ""stepSize"": ""0.01000000""
                },
                {
                  ""filterType"": ""MIN_NOTIONAL"",
                  ""minNotional"": ""0.00010000"",
                  ""applyToMarket"": true,
                  ""avgPriceMins"": 5
                },
                {
                  ""filterType"": ""ICEBERG_PARTS"",
                  ""limit"": 10
                },
                {
                  ""filterType"": ""MARKET_LOT_SIZE"",
                  ""minQty"": ""0.00000000"",
                  ""maxQty"": ""32830.82319444"",
                  ""stepSize"": ""0.00000000""
                },
                {
                  ""filterType"": ""MAX_NUM_ALGO_ORDERS"",
                  ""maxNumAlgoOrders"": 5
                },
                {
                  ""filterType"": ""MAX_NUM_ORDERS"",
                  ""maxNumOrders"": 200
                }
              ],
              ""permissions"": [
                ""SPOT"",
                ""MARGIN""
              ]
            }
         ]
        }";
    }

}
